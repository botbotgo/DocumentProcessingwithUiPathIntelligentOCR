## Document Processing with UiPath's Intelligent OCR

<a href="https://www.youtube.com/watch?v=oFHPMZehoiU
" target="_blank"><img src="thumbnail.JPG" 
alt="YouTube Link to Complete Explanation" width="400" border="10" /></a>


### Generic WorkFlow for Document Processing
<p align="center">
  <img width="219"src="https://gitlab.com/botbotgo/DocumentProcessingwithUiPathIntelligentOCR/-/raw/master/workflow.JPG?raw=true">
</p>

### Install these UiPath Packages before running the workflow

1. For ML Extractor - UiPath.DocumentUnderstanding.ML.Activities
2. For DocumentProcessing - UiPath.IntelligentOCR.Activities
3. For OCR Engine - UiPath.OmniPage.Activities